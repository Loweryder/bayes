# Setup instructions
This project uses python 2.7

# Install python deps:
```
pip install contextlib
pip install nltk
pip install numpy
pip install sklearn
pip install snowballstemmer

```

# Install nltk deps:
```
# python
> import nltk
> nltk.download()
# download the stopwords package
```

# To run, do:
```
python bayes.py
python multi_bayes.py
```

# Cython
```
./compile.sh
# python
> import bayes
> import multi_bayes
bayes.run('items.csv', 1000)
multi_bayes.run('items.csv', 2000)
```

# Varying the number of features
```
python test_parameters.py <NB|MNB> <file_name> <num_features>
```

There is a script which will try a bunch of different combinations for both multinomial NB and bernoulli NB. It saves the output in the `output` directory.
```
./run.sh
```