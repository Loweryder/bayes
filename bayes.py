#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division # force floating point division
import csv
import math
import numpy as np
import os
import re
import snowballstemmer
import time

from contextlib import contextmanager
from nltk.corpus import stopwords
from random import randrange
from sklearn.cross_validation import KFold
from sklearn.metrics import confusion_matrix, roc_curve, auc
from sklearn import preprocessing
import matplotlib.pyplot as plt


### PARAMETERS
NUM_F = 1000
CSV_FILE = 'items.csv'
ENABLE_DEBUG = False
ENABLE_PROFILE = False
ENABLE_SAMPLING = True
ENABLE_GRAPH_GENERATION = False

### GLOBALS
stops = set(stopwords.words("english"))
stemmer = snowballstemmer.stemmer('english')

@contextmanager
def measureTime(title):
  if ENABLE_PROFILE:
    t1 = time.clock()
    yield
    t2 = time.clock()
    print '[%s] took %0.3f ms' % (title, (t2-t1)*1000.0)
  else:
    yield

class NaiveBayes:
  def __init__(self, max_features):
    self.max_features = max_features
    self.H = {}
    self.vocabulary = {}
  
  def fit(self, X_train, X_target):
    if ENABLE_SAMPLING:
      titles_new = []
      categories_new = []
      count_by_category = {}
      titles_by_category = {}
      for i, title in enumerate(X_train):
        category = X_target[i]
        if not category in count_by_category:
          count_by_category[category] = 0
          titles_by_category[category] = []
        count_by_category[category] += 1
        titles_by_category[category].append(title)
      for w in sorted(count_by_category, key=count_by_category.get, reverse=True):
        avg = int(len(X_train) / len(count_by_category))
        if count_by_category[w] <= avg:
          for i in xrange(avg):
            idx = randrange(len(titles_by_category[w]))
            titles_new.append(titles_by_category[w][idx])
            categories_new.append(w)
        else:
          for i in xrange(avg):
            idx = randrange(len(titles_by_category[w]))
            categories_new.append(w)
            titles_new.append(titles_by_category[w][idx])
      X_train = np.array(titles_new)
      X_target = np.array(categories_new)

    with measureTime("fit"):
      self.P = {}
      self.N = len(X_target)
      uniq_categories = np.unique(X_target)
      for category in uniq_categories:
        self.H[category] = { 'count': 0 }
        self.P[category] = 0
      for i, category in enumerate(X_target):
        self.H[category]['count'] += 1
        item = X_train[i]
        tokens = tokenize(item)
        for token in tokens:
          if not token in self.vocabulary:
            self.vocabulary[token] = 0
          self.vocabulary[token] += 1
        for token in set(tokens):
          if not token in self.H[category]:
            self.H[category][token] = 0
          self.H[category][token] += 1
      self.top_tokens = sorted(self.vocabulary, key=self.vocabulary.get, reverse=True)[:self.max_features]

      for category in uniq_categories:
        for token in self.top_tokens:
          count = self.H[category]['count']
          c1 = count
          if token in self.H[category]:
            c1 -= self.H[category][token]
          self.P[category] += math.log((c1+1)/(count+2))

      return self

  def predict_proba(self, tokens, category):
    if category is None:
      return np.array([self.predict_proba(tokens, category) for category in self.H])

    if category not in self.H: # category wasn't in training set ?!
      return 1 / (self.N+2)

    c = self.H[category]
    p = math.log((c['count']+1) / (self.N+2))
    p += self.P[category]

    for token in tokens:
      if token in self.top_tokens:
        c1 = 0
        if token in c:
          c1 = c[token]
        p += math.log((c['count']+2) / (c['count']-c1-1+2)) # undo addition in self.P
        p += math.log((c1+1) / (c['count']+2))
      else:
        p += math.log(1/(c['count']+2))
    return math.exp(p)

  def predict(self, row):
    ps = self.predict_proba(set(tokenize(row)), None) # only check unique tokens
    return np.argmax(ps) # return index with highest probability

  def score(self, X_test, Y_test):
    with measureTime("predictions"):
      predictions = [self.predict(x) for x in X_test]
    with measureTime("calculate accuracy"):
      ncorrect = 0
      ncorrect_by_category = {}
      count_by_category = {}
      for i in np.unique(Y_test):
        ncorrect_by_category[i] = 0
        count_by_category[i] = 0
      for i, category in enumerate(predictions):
        expected_category = Y_test[i]
        count_by_category[expected_category] += 1
        if category == expected_category:
          ncorrect += 1
          ncorrect_by_category[expected_category] += 1
      macro_avgs = [v/count_by_category[k] for k,v in ncorrect_by_category.iteritems()]
      d = {}
      d['micro'] = ncorrect / float(len(Y_test))
      d['macro'] = sum(macro_avgs) / float(len(macro_avgs))
      return d

def print_stats(items_data):
  count_by_category = {}
  print "Total items: %d" % len(items_data)
  for item in items_data:
    title = item['title']
    category = item['primary_category']
    if not category in count_by_category:
      count_by_category[category] = 0
    count_by_category[category] += 1
  for w in sorted(count_by_category, key=count_by_category.get, reverse=True):
    print w, count_by_category[w]
#  print sorted(count_by_category.keys())

def tokenize(s):
  tokens = re.split('( |\/|\.|,|;|:|\(|\)|"|\?|!|®|ᴹᴰ|™|\*)', s)
  tokens = map(lambda x: x.lower(), filter(lambda x: len(x) > 1 and x not in stops and not x.isdigit(), tokens))
  return stemmer.stemWords(tokens)

# Compute ROC curve and ROC area for each class
def generate_graphs(le, nb, titles, Y, train_indices, test_indices):
  if not ENABLE_GRAPH_GENERATION:
    return
  y_pred = np.array([nb.predict_proba(set(tokenize(row)), None) for row in titles[test_indices]])
  fpr = dict()
  tpr = dict()
  roc_auc = dict()
  for i in np.unique(Y[test_indices]):
    y_score = y_pred[:,i]
    fpr[i], tpr[i], _ = roc_curve(Y[test_indices], y_score, pos_label=i)
    roc_auc[i] = auc(fpr[i], tpr[i])

    plt.figure()
    plt.plot(fpr[i], tpr[i], label='ROC curve (area = %0.2f)' % roc_auc[i])
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic for: ' + le.inverse_transform([i])[0])
    plt.legend(loc="lower right")
    plt.savefig('graphs' + os.sep + 'graph_' + str(i) + '.png')
    plt.close()

  cm = confusion_matrix(Y[test_indices], [nb.predict(x) for x in titles[test_indices]], labels=np.unique(Y))
  plt.matshow(cm)
  plt.title('Confusion matrix')
  plt.colorbar()
  plt.ylabel('True label')
  plt.xlabel('Predicted label')
  plt.savefig('graphs' + os.sep + 'confusion_matrix.png')

def run(file_name, max_features):
  items_data = list(csv.DictReader(open(file_name), delimiter=',', quotechar='"'))
  titles = np.array([item['title'] for item in items_data])
  categories = [item['primary_category'] for item in items_data]
  if ENABLE_DEBUG:
    print_stats(items_data)

  # encode categories to speed up hashing
  le = preprocessing.LabelEncoder()
  le.fit(list(set(categories))) # get unique categories
  Y = le.transform(categories)

  kf_total = KFold(len(titles), n_folds=10, indices=True, shuffle=True, random_state=1)
  with measureTime("run kfold cv"):
    L = []
    for iter, (train_indices, test_indices) in enumerate(kf_total):
      nb = NaiveBayes(max_features).fit(titles[train_indices], Y[train_indices])
      nb.fit(titles[train_indices], Y[train_indices])
      L.append(nb.score(titles[test_indices], Y[test_indices]))

      if iter == 0:
        iter += 1
        generate_graphs(le, nb, titles, Y, train_indices, test_indices)

    return L

if __name__ == '__main__':
  ENABLE_DEBUG = True
  print run(CSV_FILE, NUM_F)
