#!/usr/bin/env python

import bayes
import multi_bayes
import os
import sys

models = ['NB', 'MNB']

if len(sys.argv) < 4 or sys.argv[1] not in models:
  print "Usage: %s <%s> <file_name> <num_features>" % (sys.argv[0], "|".join(models))
  exit(1)

model_str = sys.argv[1]
file_name = sys.argv[2]
num_features = sys.argv[3]
model = bayes
if model_str == 'MNB':
  model = multi_bayes

f = open('output' + os.sep + file_name + '_' + model_str + '_' + num_features, 'wb')

L = model.run(file_name, int(num_features))
avg_micro = sum(map(lambda x: x['micro'], L)) / len(L)
avg_macro = sum(map(lambda x: x['macro'], L)) / len(L)
f.write(str({ 'micro': avg_micro, 'macro': avg_macro }) + '\n')
f.close()
