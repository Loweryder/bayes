#!/bin/bash

for x in `seq 100 100 3000`; do
  python test_parameters.py MNB items.csv $x &
done
wait

for x in `seq 100 100 2000`; do
  python test_parameters.py NB items.csv $x &
done
wait
