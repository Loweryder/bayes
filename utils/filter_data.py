#!/usr/bin/python

import csv
import nltk
import langid
d=langid.LangDetect(languages=['en','fr'])

def f(data):
  return data['title'].find('/') == -1 and data['title'].find('|') == -1 and d.detect(data['title']) == 'en'

CSV_FILE = 'sorted_items.csv'
items_data = list(csv.DictReader(open(CSV_FILE), delimiter=',', quotechar='"'))
filtered_data = filter(f, items_data)

writer = csv.writer(open('items_en.csv', 'wb'))
for data in filtered_data:
   writer.writerow([data['primary_category'], data['title']])
