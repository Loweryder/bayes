#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division # force floating point division
import csv
import math
import numpy as np
import re
import snowballstemmer

from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cross_validation import KFold
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn import preprocessing

### PARAMETERS
NUM_F = 1000
CSV_FILE = 'items.csv'
ENABLE_DEBUG = False
ENABLE_PROFILE = False

### GLOBALS
stops = set(stopwords.words("english"))
stemmer = snowballstemmer.stemmer('english')

def tokenize(s):
  tokens = re.split('( |\/|\.|,|;|:|\(|\)|"|\?|!|®|ᴹᴰ|™|\*)', s)
  tokens = map(lambda x: x.lower(), filter(lambda x: len(x) > 1 and x not in stops and not x.isdigit(), tokens))
  return stemmer.stemWords(tokens)

def run(file_name, max_features):
  items_data = list(csv.DictReader(open(file_name), delimiter=',', quotechar='"'))
  titles = [item['title'] for item in items_data]
  item_target = [item['primary_category'] for item in items_data]

  # encode categories since they're strings
  le = preprocessing.LabelEncoder()
  le.fit(list(set(item_target))) # get unique categories
  Y = le.transform(item_target)

  # convert titles to bag-of-words
  vectorizer = CountVectorizer(tokenizer=tokenize, lowercase=True, strip_accents='unicode', analyzer='word', max_features=None, binary=False)
  X = vectorizer.fit_transform(titles)
  if ENABLE_DEBUG:
    print X.shape

  kf_total = KFold(len(titles), n_folds=10, indices=True, shuffle=True, random_state=1)
  mnb = MultinomialNB()
  m = AdaBoostClassifier(base_estimator=mnb)
  return [m.fit(X[train_indices].toarray(), Y[train_indices]).score(X[test_indices].toarray(), Y[test_indices]) for train_indices, test_indices in kf_total]

if __name__ == '__main__':
  ENABLE_DEBUG = True
  print run(CSV_FILE, NUM_F)
